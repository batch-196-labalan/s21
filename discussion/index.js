// console.log("Hello world");+

// ARRAYS

// EX 1

let hobbies = ["Play piano", "play guitar", "read books"];
console.log(hobbies);

console.log(typeof hobbies); 
// type : object


// EX 2 (same data types)

let grades = [75.4,98.5,90.12,91.50];
const planets = ["Mercury", "Venues", "Earth", "Mars"];

console.log(grades);
console.log(planets);

// EX 3 (diff data types) - not a best practice 

let arraySample = ["Saitama", "One Punch Man", 25000, true];
console.log(arraySample);



// MINIACTIVITY

let dailyRoutine = ["Pray in the morning", "meditate", "play piano", "read books", "study"];
let capitalCity = [ "Beijing", "Tokyo", "Seoul", "Bangkok","Manila"];

console.log(dailyRoutine);
console.log(capitalCity);




//  VALUE OF VARIABLES CAN BE STORED IN AN ARRAY

let userName1 = "p1";
let userName2 = "p2";
let userName3 = "p3";
 
let guildMembers = [userName1, userName2, userName3];
console.log(guildMembers);


// LENGTH PROPERTY = number of elements in an array

 console.log(guildMembers.length);


// LENGTH IN STRINGS

let fullName = "Randy Orton";
console.log(fullName.length);
// total number of strings = 11


// DELETE OR REMOVE SOME ELEMENTS

/*
let userName1 = "p1";
let userName2 = "p2";
let userName3 = "p3";
 
let guildMembers = [userName1, userName2, userName3];
console.log(guildMembers);


guildMembers.length = guildMembers.length-1;
console.log(guildMembers.length);

*/


// REMOVE/shorten USING DECREMENT - not applicable with strings

capitalCity.length--;
console.log(capitalCity);


// ADDING USING INCREMENT


let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);

// result last = empty



// ACCESSING ELEMENTS OF AN ARRAY
/*
	syntax: arrayName[index]

	indices = number type

*/


/*

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);

*/
console.log(theBeatles[1]);
// result = paul 


// REASSIGNING AN ITEM IN A NEW VARIABLE USING THEIR INDEX NUM

// assign 1 by 1
theBeatles[1]="new mem";
console.log(theBeatles);


let newBand = theBeatles[1];
console.log(newBand);

// to add

newBand = newBand.concat([
  ," dragonfruit",
  " elderberry",
  " fig"
]);

console.log(newBand);





// initialize array
var arr = ["Hi", "Hello", "Bonjour", "Hola"];

// append multiple values to the array
arr.push("Salut", "Hey");

// display all values
for (var i = 0; i < arr.length; i++) {
  console.log(arr[i]);
}




// MINI ACTIVITY

let favoriteFoods = ["Tonkatsu", "Adobo", "Hamburger", "Sinigang", "Pizza"];
console.log(favoriteFoods);


favoriteFoods[3]="Sinigang na Baboy";
favoriteFoods[4]="Chicken Curry";
console.log(favoriteFoods);



// locate the last item 

console.log(favoriteFoods[favoriteFoods.length-1]);
// access the last (starts with -1)


let bullsLegend = ["Jordan", "Pippen", "Rodman", "Rose", "Scalabrine", "Lavine"];
console.log(bullsLegend[bullsLegend.length-1]);


// ADD ITEMS IN ARRAY USING INDICES

let newArr = [];
console.log(newArr);
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
newArr[1] = "Aerith Gainsborough";

console.log(newArr);



newArr[newArr.length-1] = "Tifa Lockhart";
console.log(newArr);
// replaced the previous name


newArr[newArr.length] = "Barrett Walker";
console.log(newArr);
// just add no indices required



// LOOPING OVER AN ARRAY

// SAMPLE 1

for(let index = 0; index < newArr.length; index++){

	console.log(newArr[index]);
}


// SAMPLE 2

let numArr = [5,12,30,46,40];

for(let index = 0; index < numArr.length; index++){

	if(numArr[index] % 5 === 0){
	console.log(numArr[index] + " is divisible by 5");
    }else {
	console.log(numArr[index] + " is not divisible by 5");
    }

}



// MULTIDIMENSIONAL ARRAY - arrays within an array

 let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);

console.log(chessBoard[1][5]);
// access an item w/in an array (locate the array in a row then array number in column)
// result = f2



// MINI ACTIVITY

console.log(chessBoard[7][0]);
console.log(chessBoard[5][7]);
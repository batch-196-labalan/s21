
/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.

*/

let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);	

users[users.length] = "John Cena";
console.log(users);



let itemFound;

function arrayIndex (num){
	itemFound = users[num];
	return itemFound;
}
 let index1 = arrayIndex(2)
 let index2 = arrayIndex(4)
console.log(index1);
// console.log(index2);



function itemsStored(){
	let lastItem = users[users.length-1];
	users.length--;
	return lastItem;
}

let item1 = itemsStored()
console.log(item1);
console.log(users);

// users.length--;
// console.log(users);

users[users.length-1] = "TripleH";
console.log(users);





function checkArray (users){
	if(users > 0){
		return true;
	}else {
		return false;
	}
}

let userArray = checkArray(users.length);
console.log(userArray);


isUsersEmpty = [];
console.log(isUsersEmpty);

